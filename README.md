# Support-Obfuscator

**`NOTE: This document uses the term obfuscator instead of anonymiser. At some point we will be renaming everything to anonymiser/deanononymiser. We decided to do this to avoid the use of the term obfuscation which is used elsewhere`**

## Introduction

The Obfuscator helps protect your confidential XML data by creating a key-value map which allows the deobfuscator to restore the original data in any result file. As you run the Obfuscator on each file,  it replaces every 'word' with a placeholder and a unique word ID. The replaced word is stored in a key-value map file (which we refer to here as the 'obfuscator map'), ensuring that only you can run the Deobfuscator on your files to retrieve your sensitive data.

## Obfuscating Related Files for _Compare_ or _Merge_ Operations

When obfuscating associated files (where the obfuscated versions are to be compared or merged) the same mapping of words to unique word ID should be used for each file. The Obfuscator supports this by sharing the key-value map for each associated obfuscation, adding new entries to the map when required. This helps ensure that a comparator will align words in two or more obfuscated files in the same way as if they were not obfuscated.

## Obfuscation Detail
Only XML element text-nodes and attribute values are obfuscated, names of XML elements and attributes remain unchanged, XML processing-instructions and comments also remain unchanged. For text-nodes, punctuation, whitespace characters, separators and symbols (like '$') are preserved because these characters affect the behaviour of DeltaXML's comparison algorithm.

Values for specific attributes used for CALS table processing are not obfuscated. A list of these attribute names can be found in the `src/main/resources/functions.xsl` XSLT module.

---

## How to use
The Obfuscator and Deobfuscator are implemented as XSLT 3.0 stylesheets. Follow the instructions below to obfuscate and deobfuscate XML with Saxonica's free 'Saxon-HE' XSLT processor:

### Setup

1. Ensure Java 1.8 or later is installed
2. Download latest Saxon-HE zip file from: [sourceforge.net](https://sourceforge.net/projects/saxon/files/Saxon-HE/11/Java/)
3. Unzip the download and copy the top-level Saxon directory into `src/main/resources` of this project
4. Clone this project repository onto your local machine
5. Open a terminal window, setting the current directory to `src/main/resources` inside the cloned project
6. Copy the files you wish to obfuscate into `src/main/resources` (named `test3.1.xml` and `test3.2.xml` in this sample)

### 1. Obfuscate File 1
 To obfuscate file `test3.1.xml`, type the following into the terminal window and press \<ENTER\>:

```bash
$ java -jar SaxonHE11-3J/saxon-he-11.3.jar -s:test3.1.xml -xsl:xsl/obfuscator.xsl -o:result/test3.1.obs.xml
```
After executing the command, in the terminal, you should see something like:
```text
------ Obfuscating:             file:/path/to/resources/test3.1.xml
Using previous obfuscation map: [None]
Saving obfuscated file to:      file:/path/to/resources/result/test3.1.obs.xml
Saving obfuscation map to:      file:/path/to/resources/result/obfuscate-map-1.xml
```
The Obfuscator XSLT emits a message describing the two files created.
 `[None]` is reported as the 'previous obfucation map' as this is the first time the Obfuscator has run with this '`result`' output directory.

### 2. Obfuscate File 2

 Execute a new command to obfuscate `test3.2.xml`, specify the same `result` output directory but a new output filename:
```bash
$ java -jar SaxonHE11-3J/saxon-he-11.3.jar -s:test3.2.xml -xsl:xsl/obfuscator.xsl -o:result/test3.2.obs.xml
```
After executing the command, in the terminal, you should see something like:
```text
------ Obfuscating:             file:/path/to/resources/test3.2.xml
Using previous obfuscation map: file:/path/to/resources/result/obfuscate-map-1.xml
Saving obfuscated file to:      file:/path/to/resources/result/test3.2.obs.xml
Saving obfuscation map to:      file:/path/to/resources/result/obfuscate-map-2.xml
```
Note that now the `previous obfuscation map` line now shows '`result/obfuscate-map-1.xml` 'instead of '`[None]`' and an updated obfuscation map is created: `obfuscate-map-2.xml`

### 3. Deobfuscate File 1

To deobfuscate `result/test3.1.obs.xml`, execute the following command:

```bash
$ java -jar SaxonHE11-3J/saxon-he-11.3.jar -s:result/test3.1.obs.xml -xsl:xsl/deobfuscator.xsl -o:result/test3.1.deobs.xml
```
After executing the command, in the terminal, you should see something like:

```text
------ Deobfuscating:       file:/path/to/resources/result/test3.1.obs.xml
Using obfuscation map:      file:/path/to/resources/result/obfuscate-map-2.xml
Saving restored content to: file:/path/to/resources/result/test3.1.deobs.xml
```

### 4. Deobfuscate File 2
To deobfuscate `result/test3.2.obs.xml`, execute the following command:
```bash
$ java -jar SaxonHE11-3J/saxon-he-11.3.jar -s:result/test3.2.obs.xml -xsl:xsl/deobfuscator.xsl -o:result/test3.2.deobs.xml
```
After executing the command, in the terminal, you should see something like:

```text
------ Deobfuscating:       file:/path/to/resources/result/test3.2.obs.xml
Using obfuscation map:      file:/path/to/resources/result/obfuscate-map-2.xml
Saving restored content to: file:/path/to/resources/result/test3.2.deobs.xml
```

### 5. Verify Deobfuscation Results
Firsly note that, when files `test3.1.obs.xml` and `test3.2.obs.xml` were deobfuscated above, in both cases, the latest obfuscation map `obfuscate-map-2.xml` in the `result` directory was used.

You can now verify that files `test3.1.xml` and `test3.2.xml` are the same as their respective deobfuscated files `result/test3.1.deobs.xml`  and `result/test3.2.deobs.xml`.

---
### Notes on Obfuscator Map Versioning

When the obfuscator finds any previous obfuscator maps in the specified output directory, an updated copy of the most recent obfuscator map file
is created. The numeric suffix in the obfuscator map filename (the '`2`' in `obfuscate-map-2.xml`) is incremented for each updated copy.

The deobfuscator looks in the source file directory for obfuscator maps with filenames starting with `obfuscate-map`. If more than one file match this pattern, it uses the `obfuscate-map-n.xml` file, where '`n`' is the highest number

**Tip:** When obfuscating a new set of files that are not associated with the previous set, to minimize file-size and processing, it helps to output to a different directory to ensure a 'fresh' obfuscation map is created for this new set.

---
## DeltaXML Comparison behaviour with obfuscation:

To get the same result when comparing either obfuscated or unobfuscated content in a DeltaXML product, the algorithm identifying 'words' in the Obfuscator must mirror the (ICU4J break-iterator based) algorithm within the DeltaXML product. We have endevoured to ensure this is achieved, but there will be edge-cases where a disparity occurs. 

The sample below illustrates how specific character-patterns are identified as 'words' that must be obfuscated (the text parts identified as 'words' in this sample matches the behaviour of DeltaXML products).

### Before Obfuscation - test3.xml

```xml
    <shelf>
        <book id="3123">
            <title>Slaughterhouse-Five</title>
            <author>Kurt Vonnegut</author>
            <date>23rd July 1969</date>
            <price>$100.39</price>
        </book>
        <book id="768" note="novel">
            <title>Catch-22</title>
            <author>Joseph Heller</author>
            <date>1961</date>
        </book>
        <book id="265">
            <title>Uncle Tom's Cabin</title>
            <author>Harriet Beecher Stowe</author>
            <date>1852</date>
        </book>
    </shelf>
```

### After Obfuscation

```xml
   <shelf>
      <book id="attr31">
         <title>word129-word130</title>
         <author>word131 word132</author>
         <date>word89 word17 word5</date>
         <price>$word9</price>
      </book>
      <book id="attr32" note="attr30">
         <title>word133-word134</title>
         <author>word135 word136</author>
         <date>word137</date>
      </book>
      <book id="attr33">
         <title>word138 word139 word140</title>
         <author>word141 word142 word143</author>
         <date>word144</date>
      </book>
   </shelf>
```

### Word-obfuscation detail from the sample: 

1. The Obfuscator treats `23rd` and `Tom's` each as a single 'word'.
2. Symbols like '`$`' are not obfuscated so `$100.39` is obfuscated to `$word9`, where `$` is preserved and `100.39` is obfuscated as a single 'word'.
3. The text '`Catch-22`' is treated by the Obfuscator as two words: `Catch` and `22`, separated by '`-`' which is preserved as punctuation. If the number `-22` has a separator before it, it is regarded as a negative number and thus treated as a single 'word' that is obfuscated as a whole.


