package com.deltaxml.obfuscator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import net.sf.saxon.s9api.SaxonApiException;


public class BasicTest {
	
	private Basic basic;
	private static final File projectResources= new File("target/classes");
	private static final File TestTarget= new File("target/test-classes");
	private static File inputXML, obfuscatorXSL, deobfuscatorXSL, inputOBS, resultXML, resultOBS;
	
	/**
	 * Default constructor
	 */
	public BasicTest() {		
		obfuscatorXSL= new File(projectResources, "xsl/obfuscator.xsl");
		deobfuscatorXSL= new File(projectResources, "xsl/deobfuscator.xsl");
		basic= new Basic(obfuscatorXSL, deobfuscatorXSL);
	}
	
	@Test
	public void test1() throws IOException, TransformerException, ParserConfigurationException, SAXException, SaxonApiException {
		inputXML= new File(TestTarget, "test1/test1.xml");
	    resultOBS = new File(TestTarget, "test1/test-obfuscated-1.obs");	    
	    basic.obfuscate(inputXML, resultOBS);
	    
	    Assert.assertTrue(resultOBS.exists());
	    Assert.assertEquals(Files.readAllBytes(Paths.get(TestTarget + "/test1/test-obfuscated-1.obs")),
	    		Files.readAllBytes(Paths.get(TestTarget + "/test1/expected/obfuscated-expected-1.obs")),
	    		"non-obfuscated/obfuscated differ");
	}
	
	@Test
	public void test2() throws IOException, TransformerException, ParserConfigurationException, SAXException, SaxonApiException {
		inputOBS= new File(TestTarget, "test2/test2.obs");
	    resultXML = new File(TestTarget, "test2/test-deobfuscated-2.xml");
	    basic.deobfuscate(inputOBS, resultXML);
	    
	    Assert.assertEquals(Files.readAllBytes(Paths.get(TestTarget + "/test2/test-deobfuscated-2.xml")),
	    		Files.readAllBytes(Paths.get(TestTarget + "/test2/expected/deobfuscated-expected-2.xml")),
	    		"obfuscated/deobfuscated differ");
	}
	
	@Test
	public void test3() throws IOException, TransformerException, ParserConfigurationException, SAXException, SaxonApiException {
		inputXML= new File(TestTarget, "test3/test3.xml");
	    resultOBS = new File(TestTarget, "test3/test-obfuscated-3.obs");	    
	    basic.obfuscate(inputXML, resultOBS);
	    
	    Assert.assertTrue(resultOBS.exists());
	    Assert.assertEquals(Files.readAllBytes(Paths.get(TestTarget + "/test3/test-obfuscated-3.obs")),
	    		Files.readAllBytes(Paths.get(TestTarget + "/test3/expected/obfuscated-expected-3.obs")),
	    		"non-obfuscated/obfuscated differ");
	}
	
	@Test
	public void test4() throws IOException, TransformerException, ParserConfigurationException, SAXException, SaxonApiException {
		inputOBS= new File(TestTarget, "test4/test-obfuscated-4.obs");
	    resultXML = new File(TestTarget, "test4/test-deobfuscated-4.xml");	    
	    basic.deobfuscate(inputOBS, resultXML);
	    
	    Assert.assertEquals(Files.readAllBytes(Paths.get(TestTarget + "/test4/test-deobfuscated-4.xml")),
	    		Files.readAllBytes(Paths.get(TestTarget + "/test4/expected/deobfuscated-expected-4.xml")),
	    		"obfuscated/deobfuscated differ");
	}
}
