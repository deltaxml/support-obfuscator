<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                xmlns:fn="com.deltaxml.obfuscator.functions"
                exclude-result-prefixes="xs map" 
                version="3.0">
  
  <xsl:output indent="no"/>
  <xsl:mode on-no-match="shallow-copy"/>
  <xsl:import href="functions.xsl"/>  
  
  <xsl:variable name="error">
    <message>
      Previous map not found, creating a new map.
    </message>
  </xsl:variable>
  
  <xsl:template match="/">
    <xsl:variable name="newMapFilename" as="xs:anyURI" select="fn:ofuscatorMapURI(current-output-uri(), $OBFUSCATOR_MAP.NEXT)"/>
    <xsl:variable name="previousMapFilename" as="xs:anyURI?" select="fn:ofuscatorMapURI(current-output-uri(), $OBFUSCATOR_MAP.LATEST)"/>
    <xsl:variable name="previousMapContent" select="if(doc-available($previousMapFilename)) then document($previousMapFilename) else ($error)"/>
    
    <xsl:variable name="previousWords" as="map(xs:string, xs:integer)">
      <xsl:map>
        <xsl:for-each select="$previousMapContent/obfuscation-map/words/*">
          <xsl:map-entry key="xs:string(.)" select="xs:integer(@pos)"/>
        </xsl:for-each>
      </xsl:map>
    </xsl:variable>
    
    <xsl:variable name="maxPreviousWordValue" as="xs:integer"
      select="if (map:size($previousWords) gt 0) 
          then max(for $k in map:keys($previousWords) return map:get($previousWords, $k))
        else 0"/>
    
    <xsl:variable name="currentWords" as="map(xs:string, xs:integer)">
      <xsl:map>
        <xsl:iterate select="distinct-values(//text()/fn:tokenizeText(.))">
          <xsl:param name="pos" as="xs:integer" select="$maxPreviousWordValue + 1"/>
          <xsl:if test="empty($previousWords(string(.)))">
            <xsl:map-entry key="xs:string(.)" select="$pos"/>
            <xsl:next-iteration>
              <xsl:with-param name="pos" select="$pos + 1"/>
            </xsl:next-iteration>
          </xsl:if>
        </xsl:iterate>
      </xsl:map>
    </xsl:variable>
    
    <xsl:variable name="words" as="map(xs:string, xs:integer)"
      select="map:merge(($previousWords, $currentWords), map{'duplicates':'use-first'})"/>
    
    <xsl:variable name="previousAttrs" as="map(xs:string, xs:integer)">
      <xsl:map>
        <xsl:for-each select="$previousMapContent/obfuscation-map/attrs/*">
          <xsl:map-entry key="xs:string(.)" select="xs:integer(@pos)"/>
        </xsl:for-each>
      </xsl:map>
    </xsl:variable>
    
    <xsl:variable name="maxPreviousAttrValue" as="xs:integer"
      select="if (map:size($previousAttrs) gt 0) 
          then max(for $k in map:keys($previousAttrs) return map:get($previousAttrs, $k))
        else 0"/>
    
    <xsl:variable name="currentAttrs" as="map(xs:string, xs:integer)">      
      <xsl:map>
        <xsl:iterate select="distinct-values(//@*[not(fn:preserveAttribute(.))])">
          <xsl:param name="pos" as="xs:integer" select="$maxPreviousAttrValue + 1"/>
          <xsl:if test="empty($previousAttrs(string(.)))">
            <xsl:map-entry key="xs:string(.)" select="$pos"/>
            <xsl:next-iteration>
              <xsl:with-param name="pos" select="$pos + 1"/>
            </xsl:next-iteration>
          </xsl:if>
        </xsl:iterate>
      </xsl:map>
    </xsl:variable>
    
    <xsl:variable name="attrs" as="map(xs:string, xs:integer)"
      select="map:merge(($previousAttrs, $currentAttrs), map{'duplicates':'use-first'})"/>    
    
    <xsl:message select="''"/>
    <xsl:message select="'------ Obfuscating:             ' || base-uri()"/>
    <xsl:message select="'Using previous obfuscation map: ' || ($previousMapFilename, '[None]')[1]"/>
    <xsl:message select="'Saving obfuscated file to:      ' || current-output-uri()"/>
    <xsl:message select="'Saving obfuscation map to:      ' || $newMapFilename"/>
    
    <xsl:apply-templates>
      <xsl:with-param name="words" select="$words" tunnel="yes"/>
      <xsl:with-param name="attrs" select="$attrs" tunnel="yes"/>
    </xsl:apply-templates>
    
    <xsl:result-document indent="yes" href="{$newMapFilename}" >
      <obfuscation-map>
        <words>
          <xsl:for-each select="map:keys($words)">
            <word pos="{map:get($words, .)}"><xsl:value-of select="."/></word>
          </xsl:for-each>
        </words>
        <attrs>
          <xsl:for-each select="map:keys($attrs)">
            <attr pos="{map:get($attrs, .)}"><xsl:value-of select="."/></attr>
          </xsl:for-each>
        </attrs>
      </obfuscation-map>
    </xsl:result-document>
  </xsl:template>
  
  <xsl:template match="text()">
    <xsl:param name="words" as="item()*" tunnel="yes"/>
    <xsl:value-of select="fn:obfuscateWordsInText(., $words)" />
  </xsl:template>
  
  <xsl:template match="@*[not(fn:preserveAttribute(.))]">
    <xsl:param name="attrs" as="item()*" tunnel="yes"/>
    <xsl:attribute name="{name()}" select="concat('attr', map:get($attrs, .))"/>
  </xsl:template>
  
</xsl:stylesheet>