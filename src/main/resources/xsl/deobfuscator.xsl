<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:fn="com.deltaxml.obfuscator.functions"
    exclude-result-prefixes="xs map"
    version="3.0">
    
    <xsl:output indent="no"/>
    <xsl:mode on-no-match="shallow-copy"/>
    <xsl:import href="functions.xsl"/>
    
    <xsl:variable name="previousMapUri" as="xs:anyURI?" select="fn:ofuscatorMapURI(base-uri(), $OBFUSCATOR_MAP.LATEST)"/>   
    <xsl:variable name="previousMapContent" select="document($previousMapUri)" as="document-node()?"/>
    
    <xsl:template match="/*">
        <xsl:message select="''"/>
        <xsl:message select="'------ Deobfuscating:       ' || base-uri()"/>
        <xsl:message select="
            if ($previousMapUri) then 
                'Using obfuscation map:      ' || $previousMapUri || '&#10;' || 
                'Saving restored content to: ' || current-output-uri()
            else
                fn:mapFileNotFoundMessage(base-uri())"/>
        <xsl:copy>
            <xsl:if test="$previousMapContent">
                <xsl:apply-templates select="@*, node()"/>
            </xsl:if>
        </xsl:copy>
    </xsl:template>
    
    <xsl:variable name="previousWords" as="map(xs:integer, xs:string)">
        <xsl:map>   
            <xsl:for-each select="$previousMapContent/obfuscation-map/words/*">
                <xsl:map-entry key="xs:integer(@pos)" select="xs:string(.)"/>
            </xsl:for-each>
        </xsl:map>
    </xsl:variable>
    
    <xsl:variable name="previousAttrs" as="map(xs:integer, xs:string)">
        <xsl:map>
            <xsl:for-each select="$previousMapContent/obfuscation-map/attrs/*">
                <xsl:map-entry key="xs:integer(@pos)" select="xs:string(.)"/>
            </xsl:for-each>
        </xsl:map>
    </xsl:variable>
    
    <xsl:template match="text()">
        <xsl:value-of select="fn:deobfuscateWordsInText(., $previousWords)" />
    </xsl:template>
    
    <xsl:template match="@*[not(fn:preserveAttribute(.))] except @deltaxml:*">
        <xsl:attribute name="{name()}" select="for $w in .
            return $previousAttrs(if (string-length(substring-after($w, 'attr')) eq 0) then 0 else xs:integer(substring-after($w, 'attr')))"/>
    </xsl:template>
</xsl:stylesheet>