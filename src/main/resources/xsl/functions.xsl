<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:fn="com.deltaxml.obfuscator.functions"
                exclude-result-prefixes="#all"
                expand-text="yes"
                version="3.0">
    
    <xsl:variable name="attrKeepList" as="xs:string*" 
        select="
            'colname',
            'colnum',
            'cols',
            'colsep',
            'colwidth',
            'morerows',
            'nameend',
            'namest',
            'rowheader',
            'rowsep',
            'spanname'
        "/>
    
    <xsl:variable name="OBFUSCATOR_MAP.LATEST" as="xs:boolean" select="false()"/>
    <xsl:variable name="OBFUSCATOR_MAP.NEXT" as="xs:boolean" select="true()"/>
    <xsl:variable name="ERROR_QNAME" as="xs:QName" select="QName('com.deltaxml.obfuscator', 'dxo:error')"/>
    <xsl:variable name="obfuscate-map-base-filename" as="xs:string" select="'obfuscate-map-'"/>
    
    <!-- 
         If $nextMap is true, return the uri for the next obfuscation map.
         Otherwise, returns the uri for the latest obfuscation map, or an empty-sequence if none found
    -->
    <xsl:function name="fn:ofuscatorMapURI" as="xs:anyURI?">
        <xsl:param name="base-uri" as="xs:anyURI"/>
        <xsl:param name="nextMap" as="xs:boolean"/>
        <xsl:variable name="mapNumberIncrement" as="xs:integer" select="if ($nextMap) then 1 else 0"/>
        
        <xsl:variable name="baseDirURIString" as="xs:string" select="fn:directoryURIfromURI($base-uri)"/>
        <xsl:variable name="previousMapCollection" as="document-node()*">
            <xsl:try select="collection($baseDirURIString || '?select=obfuscate-map*.xml')">
                <xsl:catch select="()"/>
            </xsl:try>
        </xsl:variable>
        <xsl:variable name="mapUriList" select="$previousMapCollection!base-uri(.)" as="xs:anyURI*"/>
        <xsl:variable name="mapNumberList" as="xs:integer*" 
            select="for $m in $mapUriList return xs:integer(substring-after(substring-before(xs:string($m), '.xml'), 'obfuscate-map-'))"/>
        <xsl:variable name="maxMapNumber" as="xs:integer" select="if (empty($mapNumberList)) then 0 else max($mapNumberList)"/>
        <xsl:variable name="resultMapFilename" as="xs:string" select="concat($obfuscate-map-base-filename, $maxMapNumber + $mapNumberIncrement, '.xml')"/>
        
        <xsl:variable name="resultMapURI" select="resolve-uri($resultMapFilename, $base-uri)"/>        
        <xsl:sequence select="
            if ($nextMap or exists($previousMapCollection)) then 
                $resultMapURI 
            else ()"/>        
    </xsl:function>
    
    <xsl:function name="fn:directoryURIfromURI" as="xs:string">
        <xsl:param name="base-uri" as="xs:anyURI"/>
        <xsl:variable name="baseUriString" as="xs:string" select="$base-uri => string()"/>
        
        <xsl:variable name="baseUriCodePoints" as="xs:integer*" select="string-to-codepoints($baseUriString)"/>
        <xsl:variable name="slashCodePoint" as="xs:integer" select="string-to-codepoints('/')"/>
        <xsl:variable name="lastSlashPos" as="xs:integer" select="index-of($baseUriCodePoints, $slashCodePoint)[last()]"/>
        <xsl:sequence select="substring($baseUriString, 1, $lastSlashPos - 1)"/>
    </xsl:function>
    
    <xsl:function name="fn:mapFileNotFoundMessage" as="xs:string">
        <xsl:param name="base-uri" as="xs:anyURI"/>
        <xsl:text>
ERROR: Could not find '{$obfuscate-map-base-filename}[n].xml' in directory: '{fn:directoryURIfromURI($base-uri)}'
</xsl:text>
    </xsl:function>
    
    <xsl:function name="fn:preserveAttribute" as="xs:boolean">
        <xsl:param name="attr" as="attribute()"/>
        <xsl:sequence select="name($attr) = $attrKeepList"/>
    </xsl:function>
    
    <!-- 
         For words, use \p{P} syntax to match word characters: Any character except:
         P Punctuation
         Z Separators
         C Others (whitespace etc.)
         S Symbols ($, £, + etc.)
    -->
    <xsl:variable name="w" as="xs:string" expand-text="no">[^\p{P}\p{Z}\p{C}\p{S}]+</xsl:variable>
    <!-- Match apostrophe chars that are used in words like O'Reilly -->
    <xsl:variable name="apos" as="xs:string">('|’)</xsl:variable>
    <xsl:variable name="number" as="xs:string">-?[0-9]+(\.[0-9]+)?((e|E)-?[0-9]+)?</xsl:variable>
    <xsl:variable name="obfuscatingRegex" as="xs:string">{$number}({$w}({$apos}{$w})?)?|{$w}({$apos}{$w})?</xsl:variable>
    <!-- e.g. match 'word22' or 'word873' -->
    <xsl:variable name="deobfuscatingRegex" as="xs:string" select="'word\d+'"/>
    
    <xsl:function name="fn:tokenizeText" as="xs:string*">
        <xsl:param name="text" as="xs:string"/>
        <xsl:analyze-string select="$text" regex="{$obfuscatingRegex}">
            <xsl:matching-substring>
                <xsl:sequence select="."/>
                <!-- for 'catch-22' case: -->
                <xsl:if test="string-length(.) gt 1 and starts-with(., '-')">
                    <xsl:sequence select="'-'"/>
                    <xsl:sequence select="substring(., 2)"/>
                </xsl:if>
            </xsl:matching-substring>
        </xsl:analyze-string>
    </xsl:function>
    
    <xsl:function name="fn:getWordsFromText" as="array(*)*">
        <xsl:param name="text" as="xs:string"/>
        <xsl:param name="forObfuscation" as="xs:boolean"/>
        
        <xsl:variable name="regex" as="xs:string" select="if ($forObfuscation) then $obfuscatingRegex else $deobfuscatingRegex"/>
        
        <xsl:analyze-string select="$text" regex="{$regex}">
            <xsl:matching-substring>
                <xsl:sequence select="[true(), .]"/>
            </xsl:matching-substring>
            <xsl:non-matching-substring>
                <xsl:sequence select="[false(), .]"/>
            </xsl:non-matching-substring>
        </xsl:analyze-string>
    </xsl:function>
    
    <xsl:function name="fn:deobfuscateWordsInText">
        <xsl:param name="text" as="xs:string"/>
        <xsl:param name="previousWords" as="map(xs:integer, xs:string)"/>
        <xsl:variable name="obfuscating" as="xs:boolean" select="false()"/>
        <xsl:variable name="wordData" as="array(*)*" select="fn:getWordsFromText($text, $obfuscating)"/>
        <xsl:variable name="strings" as="xs:string*" select="
            for $array in $wordData return 
                let $isWord := $array(1), $value := $array(2) return
                    if ($isWord) then 
                        let $wordIndex := substring($value, 5) => xs:integer() return
                            $previousWords($wordIndex)
                    else $value"/>
        <xsl:sequence select="string-join($strings, '')"/>
    </xsl:function>
    
    <xsl:function name="fn:obfuscateWordsInText">
        <xsl:param name="text" as="xs:string"/>
        <xsl:param name="previousWords" as="map(xs:string, xs:integer)"/>
        
        <xsl:variable name="obfuscating" as="xs:boolean" select="true()"/>
        <xsl:variable name="rawWordData" as="array(*)*" select="fn:getWordsFromText($text, $obfuscating)"/>
        <xsl:variable name="wordData" as="array(*)*" select="fn:fixTokensForICU4J($rawWordData)"/>
        <xsl:variable name="strings" as="xs:string*" select="
            for $array in $wordData return 
                let $isWord := $array(1), $value := $array(2) return
                    if ($isWord) then 'word' || $previousWords($value) else $value"/>
        <xsl:sequence select="string-join($strings, '')"/>
    </xsl:function>
    
    <xsl:function name="fn:fixTokensForICU4J" as="array(*)*">
        <xsl:param name="wordData" as="array(*)*"/>
        <xsl:iterate select="$wordData">
            <xsl:param name="prevIsWord" as="xs:boolean" select="false()"/>
            <xsl:variable name="isWord" as="xs:boolean" select=".(1)"/>
            <xsl:variable name="wordValue" as="xs:string" select=".(2)"/>
            
            <xsl:choose>
                <xsl:when test="$isWord and $prevIsWord">
                    <!-- for 'catch-22' case: ([true(), 'catch'], [true(), '-22']) -->
                    <xsl:sequence select="if (starts-with($wordValue, '-')) then
                            (
                                array {false(), '-'}, 
                                array {true(), substring($wordValue, 2)}
                            )  
                        else ."/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:sequence select="."/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:next-iteration>
                <xsl:with-param name="prevIsWord" select="$isWord"/>
            </xsl:next-iteration>
        </xsl:iterate>
    </xsl:function>
    
</xsl:stylesheet>