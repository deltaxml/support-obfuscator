package com.deltaxml.obfuscator;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.SAXException;

import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.XsltExecutable;
import net.sf.saxon.s9api.XsltTransformer;

public class Basic {

    private Processor p;
    private XsltTransformer xt;
    private XsltExecutable xe;
    private final File obfuscatorXSL, deobfuscatorXSL;
    
    /**
	 * Initialises this class with the supplied obfuscator/deobfuscator files
     * 
     * @param obfuscatorXSL -   File referencing the obfuscator XSLT
     * @param deobfuscatorXSL - File referencing the deobfuscator XSLT
     */
    public Basic(File obfuscatorXSL, File deobfuscatorXSL) {
    	p = new Processor(false);
    	xe= null;
		this.obfuscatorXSL= obfuscatorXSL;
		this.deobfuscatorXSL= deobfuscatorXSL;
    }
    
    /**
     * Calls common to obfuscate files
     * 
     * @param inputXML - The input XML file to be obfuscated
     * @param requestOutputFile - The obfuscated file that will be output
     */
	public final void obfuscate(File inputXML, File requestOutputFile)
			throws IOException, TransformerException, ParserConfigurationException, SAXException, SaxonApiException {
		
		common(inputXML, this.obfuscatorXSL, requestOutputFile);
	}
	
	/**
	 * Calls common to deobfuscate files
	 * 
	 * @param inputOBS - The obfuscated file that will be deobfuscated
	 * @param requestOutputFile - The deobfuscated file that will be output
	 */
	public void deobfuscate(File inputOBS, File requestOutputFile) 
			throws IOException, TransformerException, ParserConfigurationException, SAXException, SaxonApiException {
		
		common(inputOBS, this.deobfuscatorXSL, requestOutputFile);
	}
	
	/**
	 * Generic method used in obfuscation and deobfuscation methods
	 * 
	 * @param inputA - XML file
	 * @param inputB - XSL (obfuscator/deobfuscator) file
	 * @param result - result file containing obfuscated/deobfuscated content
	 */
	private void common(File inputA, File inputB, File result) throws SaxonApiException {
		
		xe = p.newXsltCompiler().compile(new StreamSource(inputB));
		
		Serializer out = p.newSerializer(result);
		out.setOutputProperty(Serializer.Property.METHOD, "xml");
		out.setOutputProperty(Serializer.Property.INDENT, "yes");
		xt = xe.load();
		xt.setSource(new StreamSource(inputA));
		xt.setDestination(out);
		xt.transform();
	}
}
